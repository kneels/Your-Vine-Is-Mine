#Your Vine Is Mine
Python script to download all videos on someone's Vine timeline. 

Usage:
---
`python yourvineismine.py [options]`

Options:
```
Required arguments:
-i  --id      Vine user ID
-p  --path    Path to download folder

Optional arguments:
-r  --revine  		Include revines [true/false] (default=true)
-d  --description  	Use the description as the filename [true/false] (default=true)
-h  --help    		Show help message
```
Example:

`python yourvineismine.py -i 1234567890 -p "C:\Downloads" -r false`
